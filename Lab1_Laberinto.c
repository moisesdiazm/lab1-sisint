/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                        // Include simpletools
#include "ping.h" 
#include "abdrive.h"                          // abdrive library

#define IR_CENTER_LED   4
#define IR_RIGHT_LED    3
#define IR_LEFT_LED     10
#define IR_CENTER_SEN   1
#define IR_RIGHT_SEN    0
#define IR_LEFT_SEN     2
#define ULTRASONIC_RIGHT 11
#define ULTRASONIC_FRONT  5


#define FRONT 1
#define FRONT_RIGHT  2
#define RIGHT 3


typedef enum CarStates {
  ROTATE_LEFT,
  ROTATE_RIGHT,
  ROTATE_RIGHT_AND_FORWARD,
  GO_FORWARD,
  DO_NOTHING
} CarStates;

// prototypes
void executeTask();
int objectDetected(int direction);
void dataAdquisition();
void changeSpeed(int, int );
void turn(void);
void rotate(float);
void straight(float );
int controlNeeded();

int irLeft, irRight, irCenter;                            // Global variables
int distanceF, distanceR;
int error = 0, change = 0;

float kp = 1.0;
float ki = 0.003;
float kd = 0.00001;
float dt = 0.1;

int main()                                    // Main function
{
  // Add startup code here.

  
  while(1)
  {
    executeTask();
    // print("irCenter: %d | irRight: %d | distanceL: %d | distanceR: %d \n", irCenter, irRight, distanceL, distanceR);
    // if (distanceL > 15 && distanceR > 15){ 
    //   drive_rampStep(0, 0);
    //   continue; //finish program execution 
    // }
    
    // // left-hand algorithm
    // // if(!irCenter || irRight == 1){
    // //   rotate(90); //turn 90 deg to the right
    // // } 
    // // if(!irCenter || distanceR >= 11){
    // //   rotate(90); //turn 90 deg to the right
    // // } 
    // // else if (distanceL < 6){     // speed correction for centered
    // //   changeSpeed(+1, -1); // increment L in one unit
    // // } else if(distanceR < 6){
    // //   changeSpeed(-1, +1); //increment R in one unit
    // // }   
    // if(!irCenter || distanceR >= 11){
    //   rotate(90); //turn 90 deg to the right
    // } 
    // else if (distanceR < 5){     // speed correction for centered
    //   changeSpeed(-1, +1); // increment L in one unit
    // } else if(distanceR > 7){
    //   changeSpeed(+1, -1); //increment R in one unit
    // }   
    // pause(100);   
  }  

}


void executeTask(){

  static CarStates currentState = DO_NOTHING;
  static CarStates nextState = DO_NOTHING;

  switch(currentState){

    case DO_NOTHING:
      drive_ramp(0,0);
      pause(200); 
      dataAdquisition();
      if(objectDetected(FRONT) && objectDetected(RIGHT)){
        currentState = ROTATE_LEFT;
      } 
      // else if(objectDetected(FRONT) && objectDetected(LEFT)){
      //   currentState = ROTATE_RIGHT;
      // } 
      else if(objectDetected(RIGHT)){
        currentState = GO_FORWARD;
      } else {
        currentState = ROTATE_RIGHT_AND_FORWARD;
      }
    break;

    case ROTATE_LEFT:
      rotate(-90);
      currentState = DO_NOTHING;

    break;

    case ROTATE_RIGHT:
      rotate(90);
      currentState = DO_NOTHING;

    break;

    case ROTATE_RIGHT_AND_FORWARD:
      rotate(90);
      drive_setMaxSpeed(25);
      straight(20.0);
      currentState = DO_NOTHING;

    break;

    case GO_FORWARD:
      dataAdquisition();

      change = 0;
      int velL = 20;
      int velR = 20;

      if(objectDetected(FRONT)){
        
        pause(300);
        currentState = DO_NOTHING;

      } else if(!objectDetected(RIGHT)){
        pause(400);
        currentState = DO_NOTHING;
      } else if(controlNeeded()){
        
        static float eant = 0;

        error = distanceF - distanceR;
        change = error * kp; 

        // change = Kp * error + (Ki * eant * dt);


        if (change > 5){
          change = 5;        
        }
        else if (change < -5){
          change = -5;
        }
        
        // eant += error;

      }
        
        
      drive_rampStep(velL + change, velR - change);
      pause(100);


      // drive_rampStep(20, 20);

      // error = 5 - distanceR;

      // change = error * kp; 

      // if (change > 5){
      //   change = 5;        
      // }
      // else if (change < -5){
      //   change = -5;
      // }

      // changeSpeed(change, -change);
      // if (){ //if no disparity between sensor measurements
      //   error = distanceF - distanceR;
        
      //   change = error * kp; 

      //   if (change > 10){
      //     change = 10;        
      //   }
      //   else if (change < -10){
      //     change = -10;
      //   }
      //   print("distanceF: %d , distanceR: %d \n", distanceF, distanceR);
      //   print("error: %d , change: %d \n", error, change);
      //   print(" ------------------------------------ \n");

      //   changeSpeed(change, -change);

      //   if(objectDetected(FRONT) || !objectDetected(RIGHT)){
      //     currentState = DO_NOTHING;
      //   }
      // }



    break;

  }

}

int controlNeeded(){
  if(objectDetected(FRONT_RIGHT) && objectDetected(RIGHT)){
    return 1;
  }

  return 0;
}

int objectDetected(int direction){
  switch(direction){

    case FRONT:
      return !irCenter;
    break;
    case FRONT_RIGHT:
      return distanceF < 15;
    break;
    case RIGHT:
      return distanceR < 15;
    break;
  }
}

void dataAdquisition(){
  distanceF = ping_cm(ULTRASONIC_FRONT);
  distanceR = ping_cm(ULTRASONIC_RIGHT);

  freqout(IR_RIGHT_LED, 1, 38000);
  irRight = input(IR_RIGHT_SEN);

  freqout(IR_CENTER_LED, 1, 38000);
  irCenter = input(IR_CENTER_SEN);
}

void changeSpeed(int incrementL, int incrementR){

  int velL = 20;
  int velR = 20;

  velL += incrementL;
  velR += incrementR;

  if(velL > 128){
    velL = 128;
  } 
  else if (velL < 0) {
    velL = 0;
  }

  if(velR > 128){
    velR = 128;
  } 
  else if (velR < 0) {
    velR = 0;
  }

  //update speed
  drive_rampStep(velL, velR); 

}


void turn(){
  drive_speed(64, -64); 

  //stop turning
  
  drive_ramp(0, 0);  
}

void rotate(float grados){
  // Returns the number of ticks for achieveing a rotation
  int ticks = (int)(1.047 * grados / 3.25);
  drive_goto(ticks, -ticks);
}

void straight(float distcm){
  
  int ticks = (int)(distcm * 10 / 3.25);
  drive_goto(ticks, ticks);
}