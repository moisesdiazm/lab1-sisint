/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                        // Include simpletools
#include "ping.h" 
#include "abdrive.h"                          // abdrive library

#define IR_CENTER_LED   4
#define IR_RIGHT_LED    3
#define IR_LEFT_LED     10
#define IR_CENTER_SEN   1
#define IR_RIGHT_SEN    0
#define IR_LEFT_SEN     2
#define ULTRASONIC_RIGHT 11
#define ULTRASONIC_LEFT  5

// prototypes
void dataAdquisition(int *, int *, int *, int *);
void changeSpeed(int, int );
void turn(void);
void rotate(float);


int main()                                    // Main function
{
  // Add startup code here.
  int irLeft, irRight, irCenter;                            // Global variables
  int distanceL, distanceR;
  
  while(1)
  {
    dataAdquisition(&distanceL, &distanceR, &irRight, &irCenter);

    print("irCenter: %d | irRight: %d | distanceL: %d | distanceR: %d \n", irCenter, irRight, distanceL, distanceR);
    if (distanceL > 15 && distanceR > 15){ 
      drive_rampStep(0, 0);
      continue; //finish program execution 
    }
    
    // left-hand algorithm
    // if(!irCenter || irRight == 1){
    //   rotate(90); //turn 90 deg to the right
    // } 
    // if(!irCenter || distanceR >= 11){
    //   rotate(90); //turn 90 deg to the right
    // } 
    // else if (distanceL < 6){     // speed correction for centered
    //   changeSpeed(+1, -1); // increment L in one unit
    // } else if(distanceR < 6){
    //   changeSpeed(-1, +1); //increment R in one unit
    // }   
    if(!irCenter || distanceR >= 11){
      rotate(90); //turn 90 deg to the right
    } 
    else if (distanceR < 5){     // speed correction for centered
      changeSpeed(-1, +1); // increment L in one unit
    } else if(distanceR > 7){
      changeSpeed(+1, -1); //increment R in one unit
    }   
    pause(100);   
  }  

}


void dataAdquisition(int *distanceL, int *distanceR, int *irRight, int *irCenter){
  *distanceL = ping_cm(ULTRASONIC_LEFT);
  *distanceR = ping_cm(ULTRASONIC_RIGHT);

  freqout(IR_RIGHT_LED, 1, 38000);
  *irRight = input(IR_RIGHT_SEN);

  freqout(IR_CENTER_LED, 1, 38000);
  *irCenter = input(IR_CENTER_SEN);

  print("%d \n", *distanceR);
}

void changeSpeed(int incrementL, int incrementR){

  static int velL = 20;
  static int velR = 20;

  velL += incrementL;
  velR += incrementR;

  if(velL > 128){
    velL = 128;
  } 
  else if (velL < 0) {
    velL = 0;
  }

  if(velR > 128){
    velR = 128;
  } 
  else if (velR < 0) {
    velR = 0;
  }

  //update speed
  drive_rampStep(velL, velR); 

}


void turn(){
  drive_speed(64, -64); 

  //stop turning
  
  drive_ramp(0, 0);  
}

void rotate(float grados){
  // Returns the number of ticks for achieveing a rotation
  int ticks = (int)(1.047 * grados / 3.25);
  drive_goto(ticks, -ticks);
}