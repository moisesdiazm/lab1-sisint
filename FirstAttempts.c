#include <simpletools.h>                    // simpletools library
#include "abdrive.h"                          // abdrive library
#include "ping.h"



int distance, setPoint, errorVal, kp, speed;  // Navigation variables


int previousDistanceL = 0, previousDistanceR = 0;
int distanceTraveledR = 0, distanceTraveledL = 0;

void rotate(float grados){
  // Returns the number of ticks for achieveing a rotation
  int ticks = (int)(1.047 * grados / 3.25);
  drive_goto(ticks, -ticks);
}

void straightSafeWithSpeed(float distcm){
   
   int currentDistanceTraveledL, currentDistanceTraveledR;
   int initialDistanceTraveledL, initialDistanceTraveledR;
   
   drive_getTicksCalc(&initialDistanceTraveledL, &initialDistanceTraveledR);
   
   currentDistanceTraveledL = initialDistanceTraveledL;
   currentDistanceTraveledR = initialDistanceTraveledR;
   
   while((currentDistanceTraveledL - initialDistanceTraveledL) < (int)(distcm * 10 / 3.25) && (currentDistanceTraveledR - initialDistanceTraveledR) < (int)(distcm * 10 / 3.25)){
    
    
    if(ping_cm(8) < 15){
     drive_speed(0,0); 
    } else {
      drive_rampStep(128, 128);
    }    
    
    drive_getTicksCalc(&currentDistanceTraveledL, &currentDistanceTraveledR);
  }   
  
  drive_ramp(0, 0);
}

void straight(float distcm){
  
  int ticks = (int)(distcm * 10 / 3.25);
  drive_goto(ticks, ticks);
}

void rPolygon(int numSides, float side){
  
  for(int i=0; i < numSides; i++){
    straightSafeWithSpeed(side);
    rotate(360/numSides); 
  }

}

void straigthSafe(float distcm){
  int ticks = (int)(distcm * 10 / 3.25);
  int step = 5;
  int i = 0;

  while(i < ticks){
    if(ping_cm(11) > 5){
      drive_goto(step, step);
      i += step;
    }
  }
}

void straightSafe2(float distcm){
  
  int ticks = (int)(distcm * 10 / 3.25);
  
 
  while((distanceTraveledL-previousDistanceL) < ticks && (distanceTraveledR-previousDistanceL)< ticks){
    
    
    previousDistanceL = distanceTraveledL;
    previousDistanceR = distanceTraveledL;
    
      
  }
}


int main()                   
{
  
  // straightSafeWithSpeed(30.0);
  rPolygon(4,20);

}
